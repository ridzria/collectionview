//
//  HomeViewController.swift
//  CollectionSlider
//
//  Created by Riaz Mohideen on 12/23/15.
//  Copyright © 2015 Riaz Mohideen. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let monsterImage = [
        UIImage(named: "Monster02Good"),
        UIImage(named: "Monster02Bad"),
        UIImage(named: "Monster02Good"),
        UIImage(named: "Monster02Bad"),
        UIImage(named: "Monster02Good"),
        UIImage(named: "Monster02Bad")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    private struct Storyboard {
        static let CellIdentifier = "Cell"
    }
    
    

}

extension HomeViewController : UICollectionViewDataSource
{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return monsterImage.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Storyboard.CellIdentifier, forIndexPath: indexPath) as! MonsterCollectionViewCell
        
        cell.monsterImageView.image = monsterImage[indexPath.row]
        cell.monsterLabel.text = "Monster"
        
        return cell
    }
}
