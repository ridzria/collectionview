//
//  MonsterCollectionViewCell.swift
//  CollectionSlider
//
//  Created by Riaz Mohideen on 12/23/15.
//  Copyright © 2015 Riaz Mohideen. All rights reserved.
//

import UIKit

class MonsterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var monsterImageView: UIImageView!
    @IBOutlet weak var monsterLabel: UILabel!
    
}
